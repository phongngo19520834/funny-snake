using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snake : MonoBehaviour {
	private  Vector2Int gridMoveDirection;
	private  Vector2Int waitGridMoveDirection;
	private int gridValue = 10;
	private int Score =0;
	private Vector2Int gridPosition;
	private float gridMoveTimer;
	private float gridMoveTimerMax;
	private float Speed;
	private float startedSpeed;
	private LevelGrid LevelGrid;
	private int snakeBodySize;
	private int SnakeSortingOrder;
	private List<Vector2Int> SnakeMovePositionList;
	public Sprite snakebodySprite; 
	private bool Dead;

	GameObject SnakebodyObject;
	//private int i = 0;
	// Use this for initialization
	public int GetScore()
	{	return Score;
	}
	public void Setup(LevelGrid levelGrid)
	{this.LevelGrid = levelGrid;
	}
	// Update is called once per frame
	private void Awake () {
		gridPosition = new Vector2Int (5, -25);
		startedSpeed = 3f;
		gridMoveTimerMax = 1f/Speed;
		gridMoveTimer = 1f/startedSpeed;
		waitGridMoveDirection = new Vector2Int (0, gridValue);
		SnakeMovePositionList = new List<Vector2Int> ();
		snakeBodySize = 1;
		SnakeSortingOrder = 3;
		Dead = false;

	}
	public bool isDead()
	{return Dead;
	}

	private void Update() {
		if (!Dead) {
			Faster ();
			HandlerInput ();
			HandlerGridMovement ();
			if(Dead) DisplayDead ();
		} 


	}
	private float RouteAngle(Vector2Int Direction){
		float n = Mathf.Atan2 (Direction.y, Direction.x) * Mathf.Rad2Deg;
		if (n < 0)
			n = n + 360;
		if (n > 360)
			n = n - 360;
		return n-90f;
	}
	private void Faster()
	{ 
		Speed = (snakeBodySize - 1)/startedSpeed + startedSpeed;
		gridMoveTimerMax = 1f/Speed;

	}
	private void DisplayDead()
	{
		for (int i = 1; i < SnakeMovePositionList.Count; i++) {
			SnakebodyObject = new GameObject("body", typeof(SpriteRenderer));
			Display(SnakebodyObject,snakebodySprite,SnakeMovePositionList[i],SnakeSortingOrder);
		}
	}
	private void HandlerGridMovement()
	{	
		
		gridMoveTimer += Time.deltaTime;
		if (gridMoveTimerMax <= gridMoveTimer) {
			gridMoveDirection = waitGridMoveDirection;
			gridPosition += gridMoveDirection;
			gridMoveTimer -= gridMoveTimerMax;
			//check death statement
			if (LevelGrid.GameOver ()) {
				Debug.Log ("Dead");
				Dead = true; return;
			}
			//add body length when eat food, add score
			SnakeMovePositionList.Insert (0, gridPosition);
			if (LevelGrid.SnakeMoved(gridPosition)) {
				snakeBodySize++;Score += 10;
			};
			//check body length
			if (SnakeMovePositionList.Count >= snakeBodySize + 1) {
				SnakeMovePositionList.RemoveAt (SnakeMovePositionList.Count - 1);
			}
			//display body
			for (int i = 1; i < SnakeMovePositionList.Count; i++) {
				SnakebodyObject = new GameObject("body", typeof(SpriteRenderer));
				Display(SnakebodyObject,snakebodySprite,SnakeMovePositionList[i],SnakeSortingOrder);
				Destroy (SnakebodyObject,gridMoveTimerMax);
			}
			//transform head
			transform.position = new Vector3 (gridPosition.x, gridPosition.y);
			transform.eulerAngles = new Vector3(0,0,RouteAngle(gridMoveDirection));
		}
	}
	private void Display(GameObject displayObject, Sprite displaySprite, Vector2Int Position, int sortingOrder)
	{
		displayObject.GetComponent<SpriteRenderer> ().sprite = displaySprite;
		displayObject.GetComponent<SpriteRenderer> ().sortingOrder = 3;
		displayObject.transform.position = new Vector3 (Position.x, Position.y);
	}
	public Vector2Int GetGridPosition ()
	{ return gridPosition;
	}
	public List<Vector2Int> GetSnakeMovePositionList()
	{ return SnakeMovePositionList;
	}
	private void HandlerInput()//moverment input
	{
		if (Input.GetKeyDown (KeyCode.UpArrow) && gridMoveDirection.y != -gridValue ) {
			waitGridMoveDirection.x = 0;
			waitGridMoveDirection.y = gridValue;
		}
		else
		if (Input.GetKeyDown (KeyCode.DownArrow) && gridMoveDirection.y != gridValue ) {
				waitGridMoveDirection.x = 0;
				waitGridMoveDirection.y = -gridValue;
		}
		else
		if (Input.GetKeyDown (KeyCode.LeftArrow) && gridMoveDirection.x != gridValue) {
					waitGridMoveDirection.x = -gridValue;
					waitGridMoveDirection.y = 0;
		}
		else
		if (Input.GetKeyDown (KeyCode.RightArrow)&& gridMoveDirection.x != -gridValue) {
						waitGridMoveDirection.x = gridValue;
						waitGridMoveDirection.y = 0;
		}


	}

}
